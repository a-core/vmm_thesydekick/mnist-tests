This repository hosts a test program for running MNIST on VMM (Vector-Matrix Multiplier). Originally developed by Xin Zhong.

### README

This README contains instructions on how to run mnist-tests assuming you are
using VMM thesydekick project. The project can
be found from: https://gitlab.com/a-core/vmm_thesydekick/a-core_thesydekick.

**WARNING**: vmm_thesydekick/a-core_thesydekick includes many updates to VMM python model.
However, these tests assume older version of VMM python model. This README
contains instructions on how to check out older version of VMM python model
to run the tests successfully.


## Initial Setup

First, clone and initialize the project:

```sh
eval `ssh-agent -c` && ssh-add
git clone git@gitlab.com:a-core/vmm_thesydekick/a-core_thesydekick.git
cd a-core_thesydekick
./init_submodules.sh
./configure
./pip3userinstall.sh
```


## Checking out older version of VMM python model

VMM python model consists of three separate entities: vmm_top, ADCmodule and
DACmodule. Each of these contains a tag `thesis-xin`, which contains the older
version of python model used with this test. Here is how to checkout correct
version of each entity:

```sh
cd Entities/vmm_top
git checkout thesis-xin
cd ../ADCmodule
git checkout thesis-xin
cd ../DACmodule
git checkout thesis-xin
cd ../..
```

**NOTE**: If you try to run other tests in the same repository now, they will
use the older version of VMM python model, which is probably not what you want.


## Running the tests

After checking out older version of VMM python model, the tests can be run
in a similar fashion to any other test.

```sh
source sourceme.csh
cd Entities/ACoreTests
./configure
make test_config=mnist-tests simulator=cocotb sim_backend=verilator
```

To run the test again after modifications, you can do it with

```sh
make clean-mnist-tests
make test_config=mnist-tests simulator=cocotb sim_backend=verilator
```

**NOTE**: Running the tests can take a very long time (for me it took about
6 hours).


