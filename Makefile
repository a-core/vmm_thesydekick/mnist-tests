DEFAULT_MAKEFILE_PATH ?= ../a-core-library

MARCH ?= rv32im
MABI ?= ilp32

CNN_HEADERS_PATH ?= ./headers
CNN_HEADERS = $(wildcard $(CNN_HEADERS_PATH)/*.h)

A_CORE_LIB_PATH ?= ../a-core-library
A_CORE_HEADERS_PATH ?= ../../../ACoreChip/chisel/include

include $(DEFAULT_MAKEFILE_PATH)/default_c.mk

INCLUDES := $(INCLUDES) -I$(CNN_HEADERS_PATH) -DACORE
LDOPTS := $(LDOPTS) -lm
