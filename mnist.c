//
// Mnist Data Process Functions.
//

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
// #include <assert.h>
#include "mnist.h"

//Reverse an int32 For Big-End
//eg: 0x11223344 --> 0x44332211
int ReverseInt(int integer)
{  
	unsigned char bits_0_7;
	unsigned char bits_8_15;
	unsigned char bits_16_23;
	unsigned char bits_24_31;

	bits_0_7   = integer & 255;         //Get the low 8-bit of int32 integer
	bits_8_15  = (integer >> 8) & 255;  //
	bits_16_23 = (integer >> 16) & 255; //
	bits_24_31 = (integer >> 24) & 255; //
	return ((int)bits_0_7 << 24) + ((int)bits_8_15 << 16) \
	     + ((int)bits_16_23 << 8) + bits_24_31;  
}

LabelArray ReadLabels(const char* filename)
{
	FILE *file_point=NULL;
	file_point = fopen(filename, "rb");
	// if(file_point==NULL)
	// 	printf("[-] <ReadLabels> Open file failed! <%s>\n",filename);
	// assert(file_point);

	int magic_number = 0;  
	int number_of_labels = 0; 
	int label_long = 10;

	fread((char*)&magic_number,sizeof(magic_number),1,file_point); 
	magic_number = ReverseInt(magic_number);  

	fread((char*)&number_of_labels,sizeof(number_of_labels),1,file_point);  
	number_of_labels = ReverseInt(number_of_labels);    

	int i;

	//LabelArray labarr=(LabelArray)malloc(sizeof(MnistLabelArray));
	LabelArray labarr=(LabelArray)malloc(sizeof(LabelArray));
	labarr->number_of_labels = number_of_labels;
	labarr->label_point = (MnistLabel*)malloc(number_of_labels*sizeof(MnistLabel));

	for(i = 0; i < number_of_labels; ++i)  
	{  
		labarr->label_point[i].label_length = 10;
		labarr->label_point[i].LabelData = (uint8_t*)calloc(label_long,sizeof(uint8_t));
		unsigned char temp = 0;  
		fread((char*) &temp, sizeof(temp),1,file_point); 
		labarr->label_point[i].LabelData[(int)temp] = 1.0;    
	}

	fclose(file_point);
	return labarr;	
}

char* IntToChar(int i)
{
	int itemp=i;
	int w=0;
	while(itemp>=10){
		itemp=itemp/10;
		w++;
	}
	char* ptr=(char*)malloc((w+2)*sizeof(char));
	ptr[w+1]='\0';
	int r;
	while(i>=10){
		r=i%10;
		i=i/10;		
		ptr[w]=(char)(r+48);
		w--;
	}
	ptr[w]=(char)(i+48);
	return ptr;
}

char * CombineStrings(char *a, char *b) 
{
	char *ptr;
	int lena=strlen(a),lenb=strlen(b);
	int i,l=0;
	ptr = (char *)malloc((lena+lenb+1) * sizeof(char));
	for(i=0;i<lena;i++)
		ptr[l++]=a[i];
	for(i=0;i<lenb;i++)
		ptr[l++]=b[i];
	ptr[l]='\0';
	return(ptr);
}
